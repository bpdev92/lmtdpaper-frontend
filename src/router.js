import Vue from 'vue';
import Router from 'vue-router';
import store from './store.js'
import profile from '@/features/profile';
import collection from '@/features/collection';
import prints from '@/features/prints';
import artists from '@/features/artists';
import addPrint from '@/features/addPrint';
import landing from '@/features/landing';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    // ...prints,
    {
      path: '/',
      component: () => import('@/features/application/ApplicationContainer.vue'),
      meta: {
        requiresAuth: true,
      },
      children: [
        ...profile,
        ...prints,
        ...artists,
        ...addPrint,
        ...collection,
      ]
    },
    ...landing,
  ],
});

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters['auth/loggedIn']) {
      next()
      return
    }
    next('/login')  
  } else {
    next() 
  }
});



export default router;