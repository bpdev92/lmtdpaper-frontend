import vue from 'vue';
import vuex from 'vuex';
import navStore from '@/features/navigation/store';
import printsStore from '@/features/prints/store';
import addPrintStore from '@/features/addPrint/store';
import authentication from '@/features/landing/store';

vue.use(vuex);

export default new vuex.Store({
  modules: {
    nav: navStore,
    prints: printsStore,
    addPrint: addPrintStore,
    auth: authentication,
  },
});
