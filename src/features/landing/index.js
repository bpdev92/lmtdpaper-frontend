import store from '@/store.js'

export default [
  {
    path: '/login',
    component: () => import('@/features/landing/LandingModule.vue'),
    beforeEnter: (to, from, next) => {
      if (!store.getters['auth/loggedIn']) {
        next()
        return
      }
      next('/')  
    
    }
  },
];
