import axios from 'axios';

const register = async (context, register) => {

    context.commit('SET_ERRORS', []);

    try {
        let response = await axios.post('http://localhost:8000/v1/auth/register', register);
        const token = response.data.data.token;
        const user = response.data.data.user;

        localStorage.setItem('token', token)
        context.commit('SET_TOKEN', token);

        localStorage.setItem('user', JSON.stringify(user))
        context.commit('SET_USER', user);

    } catch(error) {
        context.commit('SET_ERRORS', error.response.data);
    } 
};

const login = async (context, login) => {

    context.commit('SET_LOGIN_ERROR', null);

    try {
        let response = await axios.post('http://localhost:8000/v1/auth/authenticate', login);
        const token = response.data.data.token;
        const user = response.data.data.user;

        localStorage.setItem('token', token);
        context.commit('SET_TOKEN', token);

        localStorage.setItem('user', JSON.stringify(user))
        context.commit('SET_USER', user);

    } catch(error) {
        context.commit('SET_LOGIN_ERROR', 'Email or password is incorrect.');
    }
};

const logout = async (context) => {

    try {
        localStorage.removeItem('token');
        context.commit('SET_TOKEN', null);

        localStorage.removeItem('user');
        context.commit('SET_USER', null);

    } catch(error) {
        console.log(error);
    }
}


export default {
  register,
  login,
  logout,
};