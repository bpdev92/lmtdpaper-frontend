import actions from '@/features/landing/store/actions';
import getters from '@/features/landing/store/getters';
import mutations from '@/features/landing/store/mutations';

const state = {
  user: JSON.parse(localStorage.getItem('user')) || null,
  token: localStorage.getItem('token') || null,
  errors: [],
  loginError: null,
};


export default {
  state,
  actions,
  getters,
  mutations,
  namespaced: true,
};
