
const token = state => state.token;

const user = state => state.user;

const loggedIn = state => state.token !== null;

const errors = state => state.errors;

const loginError = state => state.loginError;

export default {
  token,
  user,
  errors,
  loginError,
  loggedIn,
};
