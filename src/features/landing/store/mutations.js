
export const SET_TOKEN = (state, token) => {
  state.token = token;
};

export const SET_USER = (state, user) => {
  state.user = user;
};

export const SET_ERRORS = (state, errors) => {
  state.errors = errors;
};

export const SET_LOGIN_ERROR = (state, error) => {
  state.loginError = error;
};




export default {
  SET_TOKEN,
  SET_USER,
  SET_ERRORS,
  SET_LOGIN_ERROR,
};
