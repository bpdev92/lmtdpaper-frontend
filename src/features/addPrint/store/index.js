import actions from '@/features/addPrint/store/actions';
import getters from '@/features/addPrint/store/getters';
import mutations from '@/features/addPrint/store/mutations';

const state = {
  artists: [],
  vendors: [],
  techniques: [],
  manufacturers: [],
  errors: [],
  loading: false
};


export default {
  state,
  actions,
  getters,
  mutations,
  namespaced: true,
};
