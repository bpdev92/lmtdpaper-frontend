
export const SET_ARTISTS = (state, artists) => {
  state.artists = artists;
};

export const SET_VENDORS = (state, vendors) => {
  state.vendors = vendors;
};

export const SET_TECHNIQUES = (state, techniques) => {
  state.techniques = techniques;
};

export const SET_MANUFACTURERS = (state, manufacturers) => {
  state.manufacturers = manufacturers;
};

export const SET_ERRORS = (state, errors) => {
  state.errors = errors;
};

export default {
  SET_ARTISTS,
  SET_VENDORS,
  SET_TECHNIQUES,
  SET_MANUFACTURERS,
  SET_ERRORS,
};
