
const artists = state => state.artists;

const vendors = state => state.vendors;

const techniques = state => state.techniques;

const manufacturers = state => state.manufacturers;

const errors = state => state.errors;

export default {
  artists,
  vendors,
  techniques,
  manufacturers,
  errors,
};
