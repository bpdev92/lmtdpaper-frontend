import axios from 'axios';

 const getArtists = async (context) => {
  let response = await axios.get('http://localhost:8000/v1/add-print/artists')
  context.commit('SET_ARTISTS', response.data.data);
};

const getVendors = async (context) => {
  let response = await axios.get('http://localhost:8000/v1/add-print/vendors');
  context.commit('SET_VENDORS', response.data.data);
};

const getTechniques = async (context) => {
  let response = await axios.get('http://localhost:8000/v1/add-print/techniques')
  context.commit('SET_TECHNIQUES', response.data.data);
};

const getManufacturers = async (context) => {
  let response = await axios.get('http://localhost:8000/v1/add-print/manufacturers')
  context.commit('SET_MANUFACTURERS', response.data.data);
};

const submit = async (context, print) => {
  try{
    context.commit('SET_ERRORS', []);
    await axios.post('http://localhost:8000/v1/add-print', print)
  }catch (error) {
    context.commit('SET_ERRORS', error.response.data);
  }
};

export default {
  getArtists,
  getVendors,
  getTechniques,
  getManufacturers,
  submit,
};
