
export default [
  {
    path: '/profile',
    component: () => import('@/features/profile/ProfileModule.vue'),
    children: [
      {
        path: '',
        component: () => import('@/features/profile/views/Edit.vue'),
        name: 'edit',
      },
      {
        path: 'settings',
        component: () => import('@/features/profile/views/Settings.vue'),
        name: 'settings',
      },
    ],
  },
];
