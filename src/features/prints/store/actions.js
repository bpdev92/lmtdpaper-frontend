import axios from 'axios';

//TODO: catch all these  calls

const getPrints = async (context, page) => {
  let response = await axios.post('http://localhost:8000/v1/prints', { page });
  context.commit('SET_PRINTS', response.data.data);
  context.commit('SET_PAGE', page);
  context.commit('SET_TOTAL_PAGES', response.data.meta.pagination.totalPages);
};

const getPrint = async (context, printId) => {
  let response = await axios.get('http://localhost:8000/v1/prints/' + printId);
  context.commit('SET_PRINT', response.data.data);
  context.dispatch('nav/changePageTitle', response.data.data.title, { root: true });
};

const setBrowsePage = (context, page) => {
  context.commit('SET_BROWSE_PAGE', page);
  context.dispatch('prints/getPrints', page, { root: true });
};

export default {
  getPrints,
  getPrint,
  setBrowsePage,
};
