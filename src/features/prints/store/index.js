import actions from '@/features/prints/store/actions';
import getters from '@/features/prints/store/getters';
import mutations from '@/features/prints/store/mutations';

const state = {
  prints: [],
  print: "",
  pagination : {
    totalPages: 0,
    page: 1,
  },
  browse: {
    page: 1,
  }
};


export default {
  state,
  actions,
  getters,
  mutations,
  namespaced: true,
};
