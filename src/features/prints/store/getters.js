
const prints = state => state.prints;

const print = state => state.print;

const totalPages = state => state.pagination.totalPages;

const page = state => state.pagination.page;

const browsePage = state => state.browse.page;

export default {
  prints,
  print,
  totalPages,
  page,
  browsePage,
};
