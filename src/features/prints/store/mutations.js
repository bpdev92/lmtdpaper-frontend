
export const SET_PRINTS = (state, prints) => {
  state.prints = prints;
};

export const SET_PRINT = (state, print) => {
  state.print = print;
};

export const SET_PAGE = (state, page) => {
  state.pagination.page = page;
};

export const SET_TOTAL_PAGES = (state, totalPages) => {
  state.pagination.totalPages = totalPages;
};

export const SET_BROWSE_PAGE = (state, page) => {
  state.browse.page = page;
};


export default {
  SET_PRINTS,
  SET_PRINT,
  SET_PAGE,
  SET_TOTAL_PAGES,
  SET_BROWSE_PAGE,
};
