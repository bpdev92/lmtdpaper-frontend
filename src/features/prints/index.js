
import store from '@/store.js'

export default [
  {
    path: '/prints',
    component: () => import('@/features/prints/modules/BrowseModule.vue'),
    children: [],
  },
  {
    path: '/prints/:printId',
    component: () => import('@/features/prints/modules/PrintModule.vue'),
    children: [],
    beforeEnter: async (to, from, next) => {
      await store.dispatch('prints/getPrint', to.params.printId);
      next();
    }
  },
];
