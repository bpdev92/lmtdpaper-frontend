
export default [
  {
    path: '/collection',
    component: () => import('@/features/collection/CollectionModule.vue'),
    children: [],
  },
];
