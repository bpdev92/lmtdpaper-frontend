
export const TOGGLE_DRAWER = (state, drawer) => {
  state.drawer = drawer;
};

export const TOGGLE_MINI_VARIANT = (state, miniVariant) => {
  state.miniVariant = miniVariant;
};

export const CHANGE_PAGE_TITLE = (state, pageTitle) => {
  state.pageTitle = pageTitle;
};


export default {
  TOGGLE_DRAWER,
  TOGGLE_MINI_VARIANT,
  CHANGE_PAGE_TITLE,
};
