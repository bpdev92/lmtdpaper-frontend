
const drawer = state => state.drawer;

const miniVariant = state => state.miniVariant;

const pageTitle = state => state.pageTitle;


export default {
  drawer,
  miniVariant,
  pageTitle,
};
