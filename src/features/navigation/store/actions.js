
const toggleDrawer = (context) => {
  context.commit('TOGGLE_DRAWER', !context.state.drawer);
};

const toggleMiniVariant = (context) => {
  context.commit('TOGGLE_MINI_VARIANT', !context.state.miniVariant);
};

const changePageTitle = (context, pageTitle) => {
  context.commit('CHANGE_PAGE_TITLE', pageTitle);
};

export default {
  toggleDrawer,
  toggleMiniVariant,
  changePageTitle,
};