import actions from '@/features/navigation/store/actions';
import getters from '@/features/navigation/store/getters';
import mutations from '@/features/navigation/store/mutations';

const state = {
  drawer: true,
  miniVariant: false,
  pageTitle: 'Dashboard',

};


export default {
  state,
  actions,
  getters,
  mutations,
  namespaced: true,
};
