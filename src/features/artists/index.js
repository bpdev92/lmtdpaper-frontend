
export default [
  {
    path: '/artists',
    component: () => import('@/features/artists/ArtistsModule.vue'),
    children: [],
  },
];
